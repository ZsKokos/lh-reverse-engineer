select
	consonant.lang
	, alef.script
		|| lam.script
		|| case consonant.class when 'moon' then diacritic.script else '' end
		|| consonant.script
		|| case consonant.class when 'sun' then diacritic.script else '' end
		as script
	, alef.lh_plus
		|| case consonant.class when 'sun' then '' when 'moon' then lam.lh_plus end
		|| stress1.lh_plus
		|| case stress2.ipa when 'ˌ' then '' else stress2.lh_plus end
		|| consonant.lh_plus
		|| case consonant.class when 'sun' then diacritic.lh_plus else '' end
		as lh_plus
	, alef.ipa
		|| case consonant.class when 'sun' then '' when 'moon' then lam.ipa end
		|| stress1.ipa
		|| case stress2.ipa when 'ˌ' then '' else stress2.ipa end
		|| consonant.ipa
		|| case consonant.class when 'sun' then diacritic.ipa else '' end
		as ipa
	, 'article' as class
	--, *
from
	encoding alef
	cross join encoding lam
	cross join encoding consonant
	cross join encoding stress1
	cross join encoding stress2
	cross join encoding diacritic
where
	alef.script in ('ا')
	and lam.script in ('ل')
	and consonant.class in ('sun','moon')
	and stress1.class in ('stress') and stress1.ipa in ('ˌ')
	and stress2.class in ('stress')
	and diacritic.class in ('diacritic')
	and diacritic.script = case consonant.class when 'moon' then 'ْ'  when 'sun' then 'ّ' end
