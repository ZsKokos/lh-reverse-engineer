#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys, os, sqlite3, csv, configparser, struct

os.system('chcp 65001')

sys.stdout = open(sys.stdout.fileno(), sys.stdout.mode, encoding='utf8', errors='replace', newline='\r\n', closefd=False)
sys.stderr = open(sys.stderr.fileno(), sys.stderr.mode, encoding='utf8', errors='replace', newline='\r\n', closefd=False)

#region = "Lebanon"

#dbid_filepath = os.path.join("y:\Here_Nuance-OpenNav-15Q4_v4s2-2016_03_30__13_56_52", region + ".txt")
#arabic_alphabet_filepath = 'arabic_alphabet.csv'
#article_filepath = 'article.sql'

config = configparser.ConfigParser()
config.read('lhplus.ini')
#config['region'] = {}
#config['filepath'] = {}
#config['filepath']['dbid'] = dbid_filepath
#config['filepath']['arabic_alphabet_csv'] = arabic_alphabet_filepath
#config['filepath']['article_sql'] = article_filepath
#with open('lhplus.ini', 'w') as configfile:
#	config.write(configfile)



conn = sqlite3.connect(config['filepath']['sqlite'])
conn.row_factory = sqlite3.Row
conn.execute('pragma foreign_keys=ON')

with conn:
	conn.execute('''DROP TABLE IF EXISTS dictionary_temp''')
	conn.execute('''CREATE TABLE dictionary_temp ( word VARCHAR )''')
	with open(config['filepath']['ntsampa_lhplus_dictionary'], mode='rb', ) as dict:
		character_size = 11
		header_bytes = len("$100ARG0binary mapping table header for ARG->ARS") + 8*character_size
		header = dict.read(header_bytes)
		word = dict.read(character_size)
		while(word):
			conn.execute('''INSERT INTO dictionary_temp VALUES (?)''', (word.decode().rstrip('\x00'),) )
			word = dict.read(character_size)

	conn.execute('''DROP TABLE IF EXISTS dictionary''')
	conn.execute('''CREATE TABLE dictionary ( ntsampa VARCHAR, lhplus VARCHAR )''')
	conn.execute(open(config['filepath']['convert_bin_to_dict_sql']).read())
	#conn.execute('''DROP TABLE IF EXISTS dictionary_temp''')


print ('Töltsük fel tapasztalati úton a LH+ / IPA / írott megfeletetést')
with conn:
	conn.execute('''DROP TABLE IF EXISTS encoding''')
	conn.execute('''CREATE TABLE encoding ( lang VARCHAR, lh_plus VARCHAR, script VARCHAR, class VARCHAR, ipa VARCHAR )''')
	with open(config['filepath']['arabic_alphabet_csv'], encoding='utf8') as csvfile:
		reader = csv.DictReader(csvfile, delimiter='	')
		for row in reader:
			conn.execute('''INSERT INTO encoding VALUES (?,?,?,?,?)''', (row['lang'], row['lh_plus'], row['script'], row['class'], row['ipa']) )

print ("A névelők kedvéért alakítsunk ki névelős szerkezeteket")
with conn:
	with open(config['filepath']['article_sql'], encoding='utf8') as sql:
		for row in conn.execute(sql.read()):
			conn.execute('''INSERT INTO encoding VALUES (:lang,:lh_plus,:script,:class,:ipa)''', (row['lang'], row['lh_plus'], row['script'], row['class'], row['ipa']) )

print ('Behúzzuk az eredeti dbid fájlt')
with conn:
	conn.execute('''DROP TABLE IF EXISTS dbid''')
	conn.execute('''CREATE TABLE dbid ( region VARCHAR, lang VARCHAR, original VARCHAR, lh_plus VARCHAR )''')
	with open(os.path.join(config['filepath']['dbid_folder'], config['region']['name'] + ".txt"), encoding='utf8') as fp: 
		line = fp.readline()
		while line:
			if (line[0]=='\t'):
				if (line[1]=='\t'):
				 # A forrásfájlok szóközzel elválasztva több kiejtlst is tartalmaznak
					for element in line.strip().split():
						conn.execute('''INSERT INTO dbid VALUES (?,?,?,?)''', (config['region']['name'], lang, original, element) )
				else:
					lang = line.strip()
			else:
				lang = None
				original=line.strip()
				
			line = fp.readline()

print('IPA és arab script kialakítása (SQLITE replace művelettel)')
with conn:
	conn.execute('''DROP TABLE IF EXISTS statistics''')
	conn.execute('''CREATE TABLE statistics (lh_plus VARCHAR, ipa VARCHAR, script VARCHAR, lh_plus_count INT, ipa_count INT, script_count INT)''')
	conn.execute('''ALTER TABLE dbid ADD COLUMN ipa VARCHAR''')
	conn.execute('''ALTER TABLE dbid ADD COLUMN script VARCHAR''')
	conn.execute('''UPDATE dbid SET ipa = lh_plus||'_', script = lh_plus||'_' ''')
	for row in conn.execute('''SELECT * FROM encoding ORDER BY length(lh_plus) DESC, class DESC'''):
		conn.execute('''INSERT INTO statistics VALUES (
			:substr, :ipa, :script,
			(SELECT count(*) FROM dbid WHERE lh_plus LIKE '%'||:substr||'%'),
			(SELECT count(*) FROM dbid WHERE ipa LIKE '%'||:substr||'%'),
			(SELECT count(*) FROM dbid WHERE script LIKE '%'||:substr||'%') )''', {'substr': row['lh_plus'], 'ipa': row['ipa'], 'script': row['script'] } )
		conn.execute('''UPDATE dbid SET ipa = replace(ipa,?,?), script = replace(script,?,?)''', (row['lh_plus'], row['ipa'], row['lh_plus'], row['script']) )
	conn.execute('''UPDATE dbid SET ipa = rtrim(ipa), script = rtrim(script) ''')

print('Ékezettelenítés')
with conn:
	conn.execute('''ALTER TABLE dbid ADD COLUMN script_nodiacritics VARCHAR''')
	conn.execute('''UPDATE dbid SET script_nodiacritics = script''')
	for row in conn.execute('''SELECT script FROM encoding WHERE class in ('diacritic')'''):
		conn.execute('''UPDATE dbid SET script_nodiacritics = replace(script_nodiacritics,:script,'')''', (row['script']) )
